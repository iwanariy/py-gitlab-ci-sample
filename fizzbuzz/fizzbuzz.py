# coding: utf-8


def fizzbuzz(num):
    if num % 15 == 0:
        return "Fizz Buzz"
    elif num % 3 == 0:
        return "Fizz"
    elif num % 5 == 0:
        return "Buzz"
    else:
        return num


if __name__ == "__main__":
    for num in range(1, 16):
        print(fizzbuzz(num))
